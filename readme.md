# Curso Básico de Django

Curso [platzi](https://platzi.com/cursos/django/)

## Install and Run

```sh
git checkout main
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
python manage.py runserver
```

# Curso de Django Intermedio: Testing, Static Files, Django Admin

Curso [platzi](https://platzi.com/cursos/django-intermedio/)

## Install

```sh
git checkout intermedio
python -m venv env
source env/bin.activate
pip install -r requirements.txt
python manage.py runserver
```
